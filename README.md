# trace_exec

DTrace Script for **FreeBSD**

This script monitors the system and outputs all the programs being executed, along with their command line arguments

# License
SPDX-License-Identifier: BSD-2-Clause
